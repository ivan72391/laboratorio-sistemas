<nav class=" navbar sticky-top navbar-expand-lg navbar-dark bg-danger text-light navbar-main">
        <div class="container">
            <a class="navbar-brand " href="#"><span >Laboratorio</span><span class="font-weight-bold">Sistemas</span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" ref="#fat">Servicios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" ref="<?=base_url();?>#diseño-pagina-web-gratis">Pagina Web Gratis</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" ref="#fat">Portafolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" ref="#fat">clientes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url();?>tarifa-precios-diseno-desarollo-web">Tarifas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="<?=base_url();?>contactanos">Contactenos</a>
                    </li>
                </ul>
            </div>
        </div>

    </nav>