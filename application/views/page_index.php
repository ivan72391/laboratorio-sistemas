<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="laboratorio sistemas Grupo de Diseño Web Profesional carritos de compra y administradores de contenidos. Diseñadores web con experiencia, Optimizado para Google y generar presencia en internet, para que ayuden a lograr su objetivo principal: VENDER ">
    <meta name="keywords" content="pagina web, diseño de paginas web, diseño web, tienda virtual, sitio web, servicios web, desarrollo web, diseño paginas web, diseño digital, portal web, diseño de pagina, aplicaciones web, creacion de paginas web, crear sitio web, programacion web, diseño web peru, paginas web creativas, diseño de paginas web peru, crear pagina web google, crear pagina, desarrollo de paginas web, paginas web económicas, paginas de internet">
    <meta name="author" content="laboratorio sistemas [laboratorio-sistemas.com]">
    <title>Diseño y Desarrollo Páginas Web en Puno, Perú | Laboratorio Sistemas</title>
    <?php
        $this->load->view('recursos/css');
    ?>
</head>
<body class="bg-light">
    <?php
        $this->load->view('menu/header');
        $this->load->view('menu/menu');
    ?> 
    <content>
    <?php
        $this->load->view('page/page_index');
    ?>
</content>  
<?php
    $this->load->view('recursos/js');
?> 
<?php
    $this->load->view('footer/footer');
?>    
</body>

</html>