<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="laboratorio sistemas Grupo de Diseño Web Profesional carritos de compra y administradores de contenidos. Diseñadores web con experiencia, Optimizado para Google y generar presencia en internet, para que ayuden a lograr su objetivo principal: VENDER ">
    <meta name="keywords" content="pagina web, diseño de paginas web, diseño web, tienda virtual, sitio web, servicios web, desarrollo web, diseño paginas web, diseño digital, portal web, diseño de pagina, aplicaciones web, creacion de paginas web, crear sitio web, programacion web, diseño web peru, paginas web creativas, diseño de paginas web peru, crear pagina web google, crear pagina, desarrollo de paginas web, paginas web económicas, paginas de internet">
    <meta name="author" content="laboratorio sistemas [laboratorio-sistemas.com]">
    <title>Diseño y Desarrollo Páginas Web en Puno, Perú | Laboratorio Sistemas</title>
    <?php
        $this->load->view('recursos/css');
    ?>
</head>
<body class="bg-light">
    <?php
        $this->load->view('menu/header');
        $this->load->view('menu/menu');
    ?> 
    <content>
    <?php
            


        $this->load->view('page/page_archeage');
    ?>
</content>  
<?php
    $this->load->view('recursos/js');
?> 
<?php
    $this->load->view('footer/footer');
?>    
</body>
<script type="text/javascript" charset="utf-8" async defer>
function confirmDel(){
        var agree=confirm("¿Realmente desea eliminarlo? ");
          if (agree) 
            return true;
          else
            return false;
    }
    $(document).on('click', '.btn_agregar_cosecha', function(event) {
                        event.preventDefault();
                        /* Act on the event */
                        console.log("click add");
                        if ($('#descripcion_cosecha').val().length>0&&($('#dias').val().length>0||$('#horas').val().length>0||$('#minutos').val().length>0)) {
                            var dias=($('#dias').val().length>0?$('#dias').val():'00');
                            var horas=($('#horas').val().length>0?$('#horas').val():'00');
                            var minutos=($('#minutos').val().length>0?$('#minutos').val():'00');
                                var cosecha=
                                [{
                                    descripcion_cosecha:$('#descripcion_cosecha').val(),
                                    dias:dias,
                                    horas:horas,
                                    minutos:minutos,

                                }];
                                // console.log('boletacion',boletacion);
                                $.ajax({
                                    url: "<?=base_url();?>Archeage/add",
                                    type: 'post',
                                    dataType: 'json',
                                    data: {cosecha:cosecha},
                                })
                                .done(function(data) {
                                    console.log("success",data);
                                    $('#modal_cancelar_pedido').modal('hide');
                                        location.reload();
                                    
                                    
                                })
                                .fail(function(data) {
                                    // console.log("error",data.responseText);
                                    alert('No se pudo registrar llene los campos');
                                })
                                .always(function() {
                                    // console.log("complete",data.responseText);
                                });


                           
                        }else{
                            alert('Error al Datos De boleta');
                        }
                        

                    });

    cosechas.sort(function(a, b) {
      if (a.temp_hora_restante > b.temp_hora_restante) {
        return 1;
      }
      if (a.temp_hora_restante < b.temp_hora_restante) {
        return -1;
      }
      return 0;
    });
    // console.log('cosechas',cosechas)
    var temp_cosechas='';
    var txt_cosechas='<div class="row pt-2 pb-2 mb-2 bg-secondary text-white">'+
                        '<div class="col">'+
                            'DESCRIPCION DE COSECHA'+
                        '</div>'+
                        '<div class="col-auto text-center">'+
                            'TIEMPO RESTANTE'+
                        '</div>'+
                        '<div class="col-2 text-center">'+
                            'ACCION'+
                        '</div>'+
                    '</div>';
    cosechas.forEach( function(element, index) {
        console.log('element '+element.descripcion_cosecha);
        txt_cosechas+=''+
            '<div class="row form-group">'+
                    '<div class="col">'+
                        element.descripcion_cosecha+
                    '</div>'+
                    '<div class="col-auto text-center">'+
                        element.hora_restante+
                    '</div>'+
                    '<div class="col-2 text-center">'+
                        '<a href="<?=base_url();?>Archeage/delete/'+element.id_cosecha+'" title="" onclick="return confirmDel();"><span class="btn btn-sm btn-success btn_check_cosecha" data-idcosecha="'+element.id_cosecha+'" ><span class="fa fa-check"></span></span></a>'+
                    '</div>'+
            '</div>';
    });
$('.div_cosechas').html(txt_cosechas);
// $(document).on('click', '.btn_delete_cosecha', function(event) {
//     event.preventDefault();
//     /* Act on the event */
//     var temp_id=$(this).data('idcosecha');
//     $.ajax({
//         url: '<?=base_url();?>Archeage/sss',
//         type: 'POST',
//         dataType: 'json',
//         data: {param1: 'value1'},
//     })
//     .done(function() {
//         console.log("success");
//     })
//     .fail(function() {
//         console.log("error");
//     });
    
// }); 
// $(document).on('click', '.btn_check_cosecha', function(event) {
//     event.preventDefault();
//     /* Act on the event */
//     var temp_id=$(this).data('idcosecha');
//     $.ajax({
//         url: '<?=base_url();?>Archeage/delete/'+temp_id,
//         type: 'POST',
//         dataType: 'json',
//         data: {id_cosecha:temp_id},
//     })
//     .done(function(data) {
//         console.log("success",data);
//         location.reload();
//     })
//     .fail(function() {
//         console.log("error");
//     });
    
// }); 
</script>
</html>