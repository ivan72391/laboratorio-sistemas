    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet" media="all" onload="if(media!='all')media='all'">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116135219-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-116135219-1');
    </script>
<style type="text/css">
/*font-family: 'Lato', sans-serif;*/
content {
  flex: 1;
}
    body {
        font-family: 'Quicksand'!important;
        display: flex;
  flex-direction: column;
  min-height: 100vh;
    }
    .fa-15x {
        font-size: 15rem
    }
    .h1-lato{
      font-family: 'Lato', sans-serif !important;
      font-weight: 900 !important;
    }
    .h2-lato{
      font-family: 'Lato', sans-serif !important;
      font-weight: 300 !important;
    }
    /*CHAT FC
    .chat-container{
      margin: 0;
      padding: 0;
      width: 100%;
      max-width: 340px;
      height: auto;
      position: fixed;
      bottom: 0px;
      right: 15px;
      z-index: 999;
    }
    .chat-fc-btn{
      width: 100%;
      margin: 0px;
      cursor: pointer;
      user-select: none;
      padding: 5px 0px;
      background: #dc3545;
      text-align:center;
      color: #fff;
    }
    .chat-content{
      margin: 0px;
      padding: 0px;
      background: #fff;
      display: none;
    }
</style>
