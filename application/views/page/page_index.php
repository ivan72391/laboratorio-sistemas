<div class="form-group" style="position: relative;">
        <div class="col-12" style="padding: 0;height: 550px;overflow: hidden;">
            <figure class="figure">
                <img src="https://www.analytics-sem-tutorials.de/wp-content/uploads/2017/11/startup-slider.jpg" class="figure-img img-fluid rounded " alt="A generic square placeholder image with rounded corners in a figure." style="width: 100%; height: auto; opacity: 1; position: absolute; left: -100%; right: -100%; top: -100%; bottom: -100%; margin: auto; min-height: 100%; min-width: 100%;">
                <figcaption class="figure-caption">A caption for the above image. <small>Photo &copy; Bruce’s mum</small></figcaption>
            </figure>
        </div>

    </div>
    <div class="container-fluid">
        <div class="form-group container">
            <h2 class="text-center text-capitalize  mb-4 "><span class="font-weight-bold text-uppercase h1-lato">nosotros te ayudaremos a tener éxito en Internet</span><br><small class="text-uppercase h2-lato">Dinos qué necesitas:</small></h2>
            <div class="row m-0">
                <div class="col text-capitalize text-center">
                    <a class="card p-2 h-100" href="#pagina_web" title="Pagina Web">
                        <h4 class=" text-dark">una <span class="font-weight-bold">pagina web</span></h4>
                        <article class="card-body  bg-white d-flex align-items-center">
                            <div class="w-100 text-center">
                                <span class="fa fa-tv fa-3x text-danger"></span> <span class="fa fa-mobile fa-3x text-danger"></span>
                            </div>

                        </article>
                    </a>
                </div>
                <div class="col text-capitalize text-center">
                    <a class="card p-2 h-100" href="#vender_en_internet" title="vender en internet">
                        <h4 class=" text-dark"><span class="font-weight-bold">vender</span> en internet</h4>
                        <article class="card-body  bg-white d-flex align-items-center">
                            <div class="w-100 text-center">
                                <span class="fa fa-shopping-cart fa-3x text-danger"></span>
                            </div>
                        </article>
                    </a>
                </div>
                <div class="col text-capitalize text-center">
                    <a class="card p-2 h-100" href="#posicionarme_en_internet" title="posicionarme en internet">
                        <h4 class="text-dark"><span class="font-weight-bold">posicionarme</span> en internet</h4>
                        <article class="card-body  bg-white d-flex align-items-center">
                            <div class="w-100 text-center">
                                <span class="fa fa-line-chart fa-3x text-danger"></span>
                            </div>
                        </article>
                    </a>
                </div>
                <div class="col text-capitalize text-center">
                    <a class="card p-2 h-100" href="#mejorar_la_productividad" title="mejorar la productividad">
                        <h4 class=" text-dark">Mejorar la <span class="font-weight-bold">productividad</span></h4>
                        <article class="card-body  bg-white d-flex align-items-center">
                            <div class="w-100 text-center">
                                <span class="fa fa-bar-chart fa-3x text-danger"></span>
                            </div>
                        </article>
                    </a>
                </div>
            </div>
        </div>
        <div class="bg-white form-group row pt-5 pb-5" id="pagina_web">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center pt-4">
                        <h2 class="mb-5 text-uppercase h1">una <span class="font-weight-bold">pagina web</span></h2>
                    </div>
                    <div class="col-6 text-center">
                        <span class="fa fa-tv fa-15x text-success"></span> <span class="fa fa-mobile fa-5x text-success"></span>
                    </div>
                    <div class="col-6">
                        
                        <p class="text-uppercase h1-lato"><strong class="bg-success text-white p-2">tu pagina web a medida</strong></p>
                        <p class="text-capitalize">diseño y desarollo web personalizado, porque nosotros no usamos plantillas.</p>
                        <div class="text-center">
                            <span class="btn btn-outline-success text-uppercase font-weight-bold">6 meses gratis <span class="fa fa-chevron-right"></span></span>
                        </div>
                        
                    </div>
                </div>
            </div>

        </div>
        <div class="form-group row pt-5 pb-5" id="vender_en_internet">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center pt-4">
                        <h2 class="mb-5 text-uppercase h1"><span class="font-weight-bold">vender</span> en internet</h2>
                    </div>
                    <div class="col-6 text-center">
                        <span class="fa fa-shopping-cart fa-15x text-warning"></span>
                    </div>
                    <div class="col-6">
                            <p class="text-uppercase h1-lato"><strong class="bg-warning text-white p-2">Tu tienda online</strong></p>
                            <p class="text-capitalize">integramos tu tienda a plataformas de pagos online seguras.</p>
                            <div class="text-center">
                                <span class="btn btn-outline-warning text-uppercase font-weight-bold">3 meses gratis <span class="fa fa-chevron-right"></span></span>
                            </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="bg-white form-group row pt-5 pb-5" id="posicionarme_en_internet">
            <div class="container">
                <div class="col-12 text-center pt-4">
                    <h2 class="mb-5 text-uppercase h1"><span class="font-weight-bold">posicionarme</span> en internet</h2>
                </div>
                <div class="col-6 text-center">
                    <span class="fa fa-line-chart fa-15x text-danger"></span>
                </div>
                <div class="col-6">

                </div>
            </div>

        </div>
        <div class="form-group row pt-5 pb-5" id="mejorar_la_productividad">
            <div class="container">
                <div class="col-12 text-center pt-4">
                    <h2 class="mb-5 text-uppercase h1">Mejorar la <span class="font-weight-bold">productividad</span></h2>
                </div>
                <div class="col-6 text-center">
                    <span class="fa fa-bar-chart fa-15x text-danger"></span>
                </div>
                <div class="col-6">

                </div>
            </div>

        </div>

        <!-- <div class="form-group">
            <h2 class="text-center text-uppercase text-danger mb-4 font-weight-bold">nuestros servicios</h2>
            <div class="row m-0">
                <div class="col text-capitalize">
                    <article class="border border-light bg-white rounded p-3">
                        <h4 class="text-center mb-3 bg-danger">herramienta 1</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </article>

                </div>
                <div class="col text-capitalize">
                    <article class="border border-light bg-white rounded p-3">
                        <h4 class="text-center mb-3">herramienta 2</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </article>

                </div>
                <div class="col text-capitalize">
                    <article class="border border-light bg-white rounded p-3">
                        <h4 class="text-center mb-3">herramienta 3</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </article>

                </div>
            </div>
        </div> -->
    </div>


    <div data-spy="scroll" data-target="#navbar-example2" data-offset="0">
        <h4 id="fat">@fat</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </p>
        <h4 id="mdo">@mdo</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </p>
        <h4 id="one">one</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </p>
        <h4 id="two">two</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </p>
        <h4 id="three">three</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </p>
    </div>