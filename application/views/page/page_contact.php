
	<div class="container">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="well well-sm">
	                <form class="form-horizontal" id="form-contact">
	                    <fieldset>
	                        <legend class="text-center header">Contáctanos</legend>

	                        <div class="form-group">
	                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
	                            <div class="col-md-8">
	                                <input id="txt_nombres" name="txt_nombres" type="text" placeholder="Nombres" class="form-control" autofocus>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
	                            <div class="col-md-8">
	                                <input id="txt_apellidos" name="txt_apellidos" type="text" placeholder="Apellidos" class="form-control">
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
	                            <div class="col-md-8">
	                                <input id="txt_email" name="txt_email" type="text" placeholder="E-mail" class="form-control">
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
	                            <div class="col-md-8">
	                                <input id="txt_telefono" name="txt_telefono" type="text" placeholder="Teléfono celular" class="form-control">
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
	                            <div class="col-md-8">
	                                <textarea class="form-control" id="txta_mensaje" name="txta_mensaje" placeholder="Escríbenos tu consulta o inquietud." rows="7"></textarea>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <div class="col-md-12 text-center">
	                                <button class="btn btn-danger btn-lg" id="btn-enviar-contact">Enviar</button>
	                            </div>
	                        </div>
						
	                    </fieldset>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
