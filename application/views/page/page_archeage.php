<?php echo "<script> 

var cosechas=JSON.parse(`".json_encode($cosechas)."`);
 console.log('cosechas',cosechas);

</script>"; 
?>
<div class="container">
	<div class="form-group row mt-4">
		<div class="col form-group">
			<h4 class="font-weight-bold">Lista de cosecha</h4>
		</div>
		<div class="col-auto">
			<span class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Agregar cosecha</span>
        </div>
	</div>
	
			<div class="div_cosechas">
				
			</div>
		
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><h4 class="font-weight-bold">Agregar nueva cosecha</h4></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
		

		<div class="col-12 form-group">
			<label for="descripcion_cosecha" class=" control-label">Descripcion cosecha</label>
			<textarea type="text" name="descripcion_cosecha" value="descripcion" class="form-control" id="descripcion_cosecha"></textarea>
			<span class="text-danger"></span>
		</div>
		<div class="col-12">
			<label for="tiempo_restante_cosecha" class=" control-label">Tiempo restante cosecha</label>
			<div class="row m-0">
				<div class="input-group col p-0">
			        <input type="text" class="form-control " id="dias" name="dias" placeholder="00">
			        <div class="input-group-prepend">
			          <div class="input-group-text">d.</div>
			        </div>
			     </div>
				<div class="input-group col p-0">
			        <input type="text" class="form-control col" id="horas" name="horas" placeholder="00">
			        <div class="input-group-prepend">
			          <div class="input-group-text">h.</div>
			        </div>
			     </div>
			     <div class="input-group col p-0">
			        <input type="text" class="form-control col" id="minutos" name="minutos" placeholder="00">
			        <div class="input-group-prepend">
			          <div class="input-group-text">m.</div>
			        </div>
			     </div>
				
				
			</div>
			
			<!-- <input type="text" name="tiempo_restante_cosecha" value="" class="form-control" id="tiempo_restante_cosecha" />
			<span class="text-danger"></span> -->
		</div>
	</div>
	
	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success btn_agregar_cosecha">Guardar</button>
      </div>
    </div>
  </div>
</div>