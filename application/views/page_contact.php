<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="laboratorio sistemas Grupo de Diseño Web Profesional carritos de compra y administradores de contenidos. Diseñadores web con experiencia, Optimizado para Google y generar presencia en internet, para que ayuden a lograr su objetivo principal: VENDER ">
    <meta name="keywords" content="pagina web, diseño de paginas web, diseño web, tienda virtual, sitio web, servicios web, desarrollo web, diseño paginas web, diseño digital, portal web, diseño de pagina, aplicaciones web, creacion de paginas web, crear sitio web, programacion web, diseño web peru, paginas web creativas, diseño de paginas web peru, crear pagina web google, crear pagina, desarrollo de paginas web, paginas web económicas, paginas de internet">
    <meta name="author" content="laboratorio sistemas [laboratorio-sistemas.com]">
    <title>Diseño y Desarrollo Páginas Web en Puno, Perú | Laboratorio Sistemas</title>
    <?php
        $this->load->view('recursos/css');
    ?>
</head>
<body class="bg-light">
    <?php
        $this->load->view('menu/header');
        $this->load->view('menu/menu');
    ?> 
    <content>
    <?php
        $this->load->view('page/page_contact');
    ?>
    </content>  
    <?php
        $this->load->view('recursos/js');
    ?> 
<?php
    $this->load->view('footer/footer');
?>    
</body>

<style type="text/css" media="screen">
    .header {
        color: #dc3545;
        font-size: 27px;
        padding: 10px;
    }

    .bigicon {
        font-size: 35px;
        color: #dc3545;
    }   
</style>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(document).on('click', '#btn-enviar-contact', function(event) {
            event.preventDefault();
            var dataForm = $("#form-contact").serialize();
            var nombres = $("#txt_nombres").val();
            var Apellidos = $("txt_apellidos").val();
            var email = $("#txt_email").val();
            var telefono = $("#txt_telefono").val();
            var mensaje  = $("#txta_mensaje").val(); 
            $.ajax({
                url: '<?=base_url()?>email/contact/',
                type: 'POST',
                dataType: 'json',
                data: {txt_nombres:nombres,txt_apellidos:Apellidos,txt_email:email,txt_telefono:telefono,txta_mensaje:mensaje},
            }).done(function(data) {
                console.log(data);
            }).fail(function(e) {
                console.log(e.responseText);
            });
        });
    });
</script>
</html>