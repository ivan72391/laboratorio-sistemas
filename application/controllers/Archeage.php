<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Archeage extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
    {
        parent::__construct();
        $this->load->model('Archeage_model');
    } 
	public function index()
	{
		
		$all_cosechas= $this->Archeage_model->get_all_Archeages();
		$cosechas=array();
		// echo json_encode($all_cosechas);
		foreach ($all_cosechas as $key => $value) {
			$diff=date_diff(date_create(date('Y-m-d H:i:s')), date_create($value['hora_restante']));
			$hora_final='';
			$formato=$diff->format('%R');
			$dias=$diff->format('%a');
			$horas=$diff->format('%H');
			$minutos=$diff->format('%I');
			$segundos=$diff->format('%S');
			if ($dias>0) {
				$hora_final.=$dias.' d ';
			}
			if ($horas>0) {
				$hora_final.=$horas.' h ';
			}
			if ($minutos>0) {
				$hora_final.=$minutos.' m ';
			}
			if ($minutos>0) {
				$hora_final.=$segundos.' s ';
			}
			$cosechas[]= array(
				'id_cosecha'=>$value['id_cosecha'],
				 'descripcion_cosecha'=>$value['descripcion_cosecha'],
				 'temp_hora_restante'=>$value['hora_restante'],
				 'hora_restante'=>$formato.$hora_final,
				 'estado_cosecha'=>$value['estado_cosecha'],
			);
		}
		// $diff->format("%R%a d %H h %I m %S s"),
		$data['cosechas'] =$cosechas;
		$this->load->view('page_archeage',$data);
	}
	function add(){   
		$data=$this->input->post('cosecha');
        // echo var_dump($data);
		if(!empty($data))     
        {   

			$dias=$data[0]['dias'].' days';
			$horas=$data[0]['horas'].' hours';
			$minutos=$data[0]['minutos'].' minutes';


        $fecha = date('Y-m-d H:i:s');
            $nuevafecha = strtotime ( '+'.$dias.' '.$horas.' '.$minutos.'' , strtotime ( $fecha ) ) ;
            $nuevafecha = date ( 'Y-m-d H:i:s' , $nuevafecha );   



          $params = array(
                    'descripcion_cosecha' => $data[0]['descripcion_cosecha'],
                    'hora_restante' => $nuevafecha,
                );

                $result = $this->Archeage_model->add_cosecha($params);
                echo json_encode($result);
            


        }
        else
        {            
            echo json_encode('Error');
        }
    } 
    function delete($temp_id){
    	// $data=$this->input->post('id_cosecha');
    	// var_dump($data);
    	$result = $this->Archeage_model->delete_Archeage($temp_id);
    	// echo json_encode($result);
    	redirect('Archeage');
    }
}
