<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {
	
	function __construct(){
        parent::__construct();
   	}

	public function index(){
		
		$this->load->view('page_contact');
	}

	public function contact(){
		$data = [];
		$nombres 		= $this->input->post('txt_nombres');
		$apellidos 		= $this->input->post('txt_apellidos');
		$telefono 		= $this->input->post('txt_telefono');
		$email 	  		= $this->input->post('txt_email');
		$mensaje  		= $this->input->post('txta_mensaje');

		if ( !empty( trim($nombres)) ) {
			if ( !empty( trim($email)) && $this->is_valid_email( trim($email) )  ) {
				if ( strlen( trim($mensaje)) > 16 ) {
					$data['response'] 		= "success";
					$data['error_level']	= null;
					$data['message']		= "Su consulta a sido enviado con éxito, Durante el transcurso del día un especialista le responderá a su consulta.";
				}else{
					$data['response'] 		= "error";
					$data['error_level']	= 3;
					$data['message']		= "Sustente mejor su consulta.";
				}
			}else{
				$data['response'] 		= "error";
				$data['error_level']	= 2;
				$data['message']		= "E-mail inválido.";
			}
		}else{
			$data['response'] 		= "error";
			$data['error_level']	= 1;
			$data['message']		= "Nombres no válidos.";
		}
		echo json_encode($data);

	}

	function is_valid_email($str){
	  $matches = null;
	  return (1 === preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $str, $matches));
	}


}
